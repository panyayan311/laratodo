<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TaskController@index');

Auth::routes();

Route::get('/task','TaskController@add');
Route::post('/task','TaskController@create');

Route::get('/task/{task}','TaskController@edit');
Route::post('/task/{task}','TaskController@update');

//edit user page route
Route::get('/profile', 'ProfileController@show');
Route::get('/profile/{user}', ['as' => 'profile.profile_edit', 'uses' => 'ProfileController@profile_edit']);
Route::patch('/profile/{user}/update', ['as' => 'profile.update' ,'uses' => 'ProfileController@update']);
