@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="well well-sm">
                    <div class="media">
                        <a class="thumbnail pull-left" href="#">
                            <img class="media-object" src="http://www.shoshinsha-design.com/wp-content/uploads/2017/11/%E3%82%B9%E3%83%A9%E3%82%A4%E3%83%A0%E3%82%A2%E3%82%A4%E3%82%B3%E3%83%B3-545x460.png">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{ $user -> name}}</h4>
                            <h4 class="media-heading">{{ $user -> email }}</h4>
                            <form action="/profile/{{$user->id}}">
                                <button type="submit" name="profile_edit" class="btn btn-primary">Edit</button>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection()