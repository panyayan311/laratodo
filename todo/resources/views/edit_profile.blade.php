@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form-group" method="post" action="{{route('profile.update', $user)}}">
            {{ csrf_field() }}
            {{ method_field('patch') }}

            <input class="form-control" type="text" name="name"  value="{{ $user->name }}" />
            <br>
            <input class="form-control" type="email" name="email"  value="{{ $user->email }}" />
            <br>
            <button type="submit" class="btn btn-primary">Update profile</button>
        </form>
    </div>
    @endsection()